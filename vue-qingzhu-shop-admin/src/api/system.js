import request from '@/utils/request'

export function getAllRoles() {
  return request({
    url: '/system/admin/roles',
    method: 'get'
  })
}

export function searchAdmin(keyword) {
  return request({
    url: "/system/admin/?keyword="+keyword,
    method: 'get'
  })
}

export function editRoles(id,roles) {
  return request({
    url:'/system/admin/role/edit/'+ id + '?roleIds=' + roles,
    method:'put'
  })
}

export function getAdmin(id) {
  return request({
    url:'/system/admin/'+id,
    method:'get'
  })
}

export function postAdmin(admin) {
  return request({
    url:'/system/admin/',
    method:'post',
    data:admin
  })
}

export function putAdmin(admin) {
  return request({
    url:'/system/admin/',
    method:'put',
    data:admin
  })
}

export function deleteAdmin(id) {
  return request({
    url:'/system/admin/'+id,
    method:'delete'
  })
}

