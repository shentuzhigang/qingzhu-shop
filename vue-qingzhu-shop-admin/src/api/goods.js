import request from '@/utils/request'

export function pageGoodsQuery(data) {
  return request({
    url: '/admin/goods/query',
    method: 'get',
    params:data
  })
}
