package cn.edu.zstu.qingzhu.qingzhushop.controller;


import cn.edu.zstu.qingzhu.qingzhushop.entity.Notice;
import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import cn.edu.zstu.qingzhu.qingzhushop.service.INoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-08-19
 */
@RestController
@RequestMapping("/notice/basic")
@Api(tags = "通知基本接口")
public class NoticeController {
    @Autowired
    INoticeService iNoticeService;

    /**
     * 获取单个通知
     * @param id 通知ID
     * @return Object
     */
    @ApiOperation(value = "获取单个通知",notes = "获取单个通知")
    @RequestMapping(value = "/{id}",method = {RequestMethod.GET})
    public Object getNotice(@PathVariable(value = "id") Integer id){
        Notice notice = iNoticeService.getById(id);
        if(notice == null){
            return ResponseBean.error("未找到此通知");
        }
        return notice;
    }

    /**
     * 添加单个通知
     * @param notice 通知实体
     * @return Object
     */
    @ApiOperation(value = "添加单个通知",notes = "添加单个通知")
    @RequestMapping(value = "/",method = {RequestMethod.POST})
    public Object postNotice(@RequestBody Notice notice){
        boolean save = iNoticeService.save(notice);
        if(!save){
            return ResponseBean.error("保存失败");
        }
        return notice;
    }

    /**
     * 更新单个通知
     * @param id 通知 ID
     * @param notice 通知实体
     * @return Object
     */
    @ApiOperation(value = "更新单个通知",notes = "更新单个通知")
    @RequestMapping(value = "/{id}",method = {RequestMethod.PUT})
    public Object putNotice(@PathVariable(value = "id") Integer id,@RequestBody Notice notice){
        notice.setId(id);
        boolean update = iNoticeService.updateById(notice);
        if(!update){
            return ResponseBean.error("更新失败");
        }
        return notice;
    }

    /**
     * 删除单个通知
     * @param id 通知ID
     * @return Object
     */
    @ApiOperation(value = "删除单个通知",notes = "删除单个通知")
    @RequestMapping(value = "/{id}",method = {RequestMethod.DELETE})
    public Object deleteNotice(@PathVariable(value = "id") Integer id){
        boolean remove = iNoticeService.removeById(id);
        if(!remove){
            return ResponseBean.error("删除失败");
        }
        return null;
    }
}

