package cn.edu.zstu.qingzhu.qingzhushop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-26 15:13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Vue路由配置")
public class VueRouteConfig {
    /*
        RouteConfig {
          path: string
          name?: string
          component?: Component
          components?: Dictionary<Component>
          redirect?: RedirectOption
          alias?: string | string[]
          children?: RouteConfig[]
          meta?: any
          beforeEnter?: NavigationGuard
          props?: boolean | Object | RoutePropsFunction
          caseSensitive?: boolean
          pathToRegexpOptions?: PathToRegexpOptions
        }
    */
    @ApiModelProperty(name = "路径", value = "路径")
    private String path;
    @ApiModelProperty(name = "组件", value = "组件")
    private String component;
    @ApiModelProperty(name = "名称", value = "名称")
    private String name;
    @ApiModelProperty(name = "重定向", value = "重定向")
    private String redirect;
    @ApiModelProperty(name = "别名", value = "别名")
    private String alias;
    @ApiModelProperty(name = "元", value = "元")
    private Meta meta;
    @ApiModelProperty(name = "子菜单", value = "子菜单")
    private List<VueRouteConfig> children;

    public static List<VueRouteConfig> build(List<Resource> resources) {
        Map<Integer, Resource> map = new HashMap<>();
        Map<Integer, List<Integer>> listMap = new HashMap<>();
        for (Resource resource : resources) {
            map.put(resource.getId(), resource);
            if (listMap.get(resource.getParentId()) == null) {
                listMap.put(resource.getParentId(), new ArrayList<>());
            }
            listMap.get(resource.getParentId()).add(resource.getId());
        }
        List<VueRouteConfig> list = new ArrayList<>();
        for (Resource resource : resources) {
            if (resource.getParentId() == 0) {
                list.add(findChildren(map, listMap, map.get(resource.getId())));
            }
        }
        return list;
    }

    private static VueRouteConfig findChildren(Map<Integer, Resource> map,
                                               Map<Integer, List<Integer>> listMap,
                                               Resource resource) {
        VueRouteConfig vueRouteConfig = new VueRouteConfig();
        vueRouteConfig.setName(resource.getName());
        vueRouteConfig.setComponent(resource.getComponent());
        Meta meta = new Meta();
        meta.setIcon(resource.getIcon());
        meta.setKeepAlive(resource.getKeepAlive());
        meta.setRequireAuth(resource.getRequireAuth());
        vueRouteConfig.setMeta(meta);
        vueRouteConfig.setPath(resource.getPath());
        List<VueRouteConfig> vueRouteConfigs = new ArrayList<>();
        List<Integer> integers = listMap.get(resource.getId());
        if (integers == null) {
            return vueRouteConfig;
        }
        for (Integer id : integers) {
            vueRouteConfigs.add(findChildren(map, listMap, map.get(id)));
        }
        vueRouteConfig.setChildren(vueRouteConfigs);
        return vueRouteConfig;
    }
}
