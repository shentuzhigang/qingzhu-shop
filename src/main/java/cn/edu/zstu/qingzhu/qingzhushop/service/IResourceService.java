package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Resource;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-03-08 19:44
 */
public interface IResourceService extends IService<Resource> {
    List<Resource> getAllResource();

    List<Resource> getAllResourcesWithChildren();
}
