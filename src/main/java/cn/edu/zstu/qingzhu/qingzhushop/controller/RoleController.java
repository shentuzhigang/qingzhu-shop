package cn.edu.zstu.qingzhu.qingzhushop.controller;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Role;
import cn.edu.zstu.qingzhu.qingzhushop.service.IRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-25 10:46
 */
@RestController
@RequestMapping("/role")
@Api(tags = "角色功能接口")
public class RoleController extends BaseRestController<IRoleService, Role,Integer> {
    @Autowired
    IRoleService iRoleService;
    /**
     *
     * @return
     */
    @ApiOperation(value = "获取所有角色",notes = "获取所有角色",response = Object.class)
    @RequestMapping(value = "/list",method = {RequestMethod.GET})
    public Object getAllRole(){
        return iRoleService.list();
    }

    /**
     *
     * @param roleId
     * @return
     */
    @ApiOperation(value = "根据角色ID获取资源ID列表",notes = "根据角色ID获取资源ID列表",response = List.class)
    @RequestMapping(value = "/resourceids/{roleId}",method = {RequestMethod.GET})
    public List<Integer> getResourceIdsByRoleId(@PathVariable("roleId") Integer roleId){
        return iRoleService.getResourceIdsByRoleId(roleId);
    }

    @RequestMapping(value = "/permit/{roleId}",method = {RequestMethod.PUT})
    public List<Integer> permitRoleWithResourceIds(@PathVariable("roleId") Integer roleId,
                                                   @RequestParam("resourceIds") Integer[] resourceIds){
        iRoleService.updateResourceRole(roleId,resourceIds);
        return iRoleService.getResourceIdsByRoleId(roleId);
    }
}
