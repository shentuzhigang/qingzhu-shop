package cn.edu.zstu.qingzhu.qingzhushop.entity;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-11 21:33
 */
public interface IGenerative<T> {
    T generate();

    T generate(T wrapper);
}
