package cn.edu.zstu.qingzhu.qingzhushop.configure;

import cn.edu.zstu.qingzhu.qingzhushop.properties.EmailProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-08-04 19:46
 */
@EnableConfigurationProperties(EmailProperties.class)
public class EmailConfiguration {
}
