package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-25 13:34
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "资源角色映射")
public class ResourceRole {
    //ID
    @ApiModelProperty(name = "资源角色映射ID", value = "资源角色映射ID")
    @TableId(value = "id", type = IdType.AUTO)
    private int id;
    //角色ID
    @ApiModelProperty(name = "角色ID", value = "角色ID")
    private int rid;

    //资源ID
    @ApiModelProperty(name = "资源ID", value = "资源ID")
    private int sid;

}
