package cn.edu.zstu.qingzhu.qingzhushop.mapper;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Goods;
import cn.edu.zstu.qingzhu.qingzhushop.entity.Seckill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * FileName: SeckillMapper
 *
 * @Author: LvYibin
 * @Date: 2021/5/17 09:04
 * @Description:
 */
@Mapper
@Component
public interface SeckillMapper extends BaseMapper<Seckill> {
}
