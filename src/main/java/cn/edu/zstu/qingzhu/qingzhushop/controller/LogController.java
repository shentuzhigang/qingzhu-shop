package cn.edu.zstu.qingzhu.qingzhushop.controller;


import cn.edu.zstu.qingzhu.qingzhushop.entity.Log;
import cn.edu.zstu.qingzhu.qingzhushop.service.ILogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-08-02
 */
@RestController
@RequestMapping("/admin/log")
@Api(tags = "日志功能接口")
public class LogController{
    @Autowired
    ILogService iLogService;
    @RequestMapping(value = "/list",method = {RequestMethod.GET})
    public Object pageLog(@ApiParam(name = "page",value = "页数")
                              @RequestParam(defaultValue = "1") Integer page,
                          @ApiParam(name = "size",value = "记录数")
                              @RequestParam(defaultValue = "10") Integer size,
                          @ApiParam(name = "keyword",value = "关键字")
                              @RequestParam(required = false) String keyword,
                          @ApiParam(name = "status",value = "状态")
                              @RequestParam(required = false) String status,
                          @ApiParam(name = "date",value = "时间范围")
                              @RequestParam(required = false) Timestamp[] date,
                          @ApiParam(name = "sort",value = "排序")
                              @RequestParam(required = false) String sort){

        QueryWrapper<Log> logQueryWrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(keyword)){
            logQueryWrapper.like("operator",keyword);
        }
        if(!StringUtils.isEmpty(status)){
            logQueryWrapper.eq("status",status);
        }
        if(!StringUtils.isEmpty(sort)){
            try{
                Pattern pattern = Pattern.compile("([\\+|-][a-zA-Z]+)");
                Matcher matcher = pattern.matcher(sort);
                while (matcher.find()) {
                    String str = matcher.group(1);
                    if(str.charAt(0)=='+'){
                        logQueryWrapper.orderByAsc(str.substring(1));
                    }else if(str.charAt(0)=='-'){
                        logQueryWrapper.orderByDesc(str.substring(1));
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if(date!=null && date.length==2){
            logQueryWrapper.between("time",date[0],date[1]);
        }

        return iLogService.page(new Page<>(page,size),logQueryWrapper);
    }
}

