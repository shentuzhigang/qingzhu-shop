package cn.edu.zstu.qingzhu.qingzhushop.mapper;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Goods;
import cn.edu.zstu.qingzhu.qingzhushop.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-11-08
 */
@Mapper
@Component
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {
    @Select("select * from order_detail o where o.order_id=#{orderId}")
    List<OrderDetail> getGoodsListByOrderId(Integer orderId);

    List<Goods> getOrderDetailWithGoodsByOrderId(Integer orderId);
}
