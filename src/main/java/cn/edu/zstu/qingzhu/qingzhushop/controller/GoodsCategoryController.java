package cn.edu.zstu.qingzhu.qingzhushop.controller;


import cn.edu.zstu.qingzhu.qingzhushop.entity.GoodsCategory;
import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import cn.edu.zstu.qingzhu.qingzhushop.service.IGoodsCategoryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-11-08
 */
@RestController
@CacheConfig(cacheNames = "GoodsCategory")
@RequestMapping("/goodsCategory")
@Api(tags = "商品类型接口")
public class GoodsCategoryController {
    @Autowired
    private IGoodsCategoryService iGoodsCategoryService;

    /**
     * 获取所有活动
     * @return
     */
    @GetMapping("/list")
    @Cacheable(value = "GoodsCategoryList",keyGenerator = "simpleKeyGenerator")
    @ResponseBody
    public ResponseBean getGoodsCategoryList() {
        List<GoodsCategory> list = iGoodsCategoryService.listWithTree();
        return ResponseBean.success("GoodsCategory", list);
    }
}

