package cn.edu.zstu.qingzhu.qingzhushop.mapper;


import cn.edu.zstu.qingzhu.qingzhushop.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface GoodsMapper extends BaseMapper<Goods> {

    Goods selectByGoodsId(Integer id);

    List<Goods> selectByGoodsName(String goods_Name);

    int updateByIdSelective(Goods goods);

    List<Goods> selectByActivityId(Integer activityId);

    int addGood2Activity(@Param("goodIds") Integer goodIds, @Param("activityId") Integer activityId);

    int rmvGoodFromAct(@Param("goodsId") Integer goodsId, @Param("activityId") Integer activityId);

    List<Integer> selectIdListByGoodsCategoryId(Integer id);

    List<Integer> selectAllGoodsId();
}
