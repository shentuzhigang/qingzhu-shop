package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-12-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Order对象", description = "")
@TableName(value = "`order`")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(name = "Order ID", value = "Order ID", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "金额")
    private String amount;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "数量")
    @TableField(value = "totalnumber")
    private Integer totalnumber;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "顾客ID")
    private Integer customerId;

    @TableField(exist = false)
    private User customer;

    @ApiModelProperty(value = "商家ID")
    private Integer employeeId;

    @TableField(exist = false)
    private User employee;

    @ApiModelProperty(value = "信息")
    private String message;

    @TableField(exist = false)
    private List<Goods> cartList;//订单对应的购物车数据

    @TableField(exist = false)
    private List<OrderDetail> detail;  //detail包含所有的购物车id

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
