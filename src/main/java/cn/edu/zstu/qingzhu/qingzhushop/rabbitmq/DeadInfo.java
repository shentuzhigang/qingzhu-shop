package cn.edu.zstu.qingzhu.qingzhushop.rabbitmq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * FileName: DeadInfo
 *
 * @Author: LvYibin
 * @Date: 2020/7/21 14:35
 * @Description:
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DeadInfo implements Serializable {
    private Integer id;
    private String name;
}
