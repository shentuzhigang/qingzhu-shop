package cn.edu.zstu.qingzhu.qingzhushop.service.impl;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Notice;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.NoticeMapper;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.UserMapper;
import cn.edu.zstu.qingzhu.qingzhushop.service.INoticeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-08-19
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {

    @Autowired
    UserMapper userMapper;
    @Autowired
    NoticeMapper noticeMapper;

    @Override
    public List<Notice> listNoticeWithReceiver() {
        List<Notice> list = noticeMapper.selectList(new QueryWrapper<>());
        for (Notice notice : list) {
            if (notice.getReceiveUid() != 0) {
                notice.setReceiver(userMapper.selectById(notice.getReceiveUid()));
            }
        }
        return list;
    }

    @Override
    public List<Notice> getUnpublishedNotice() {
        QueryWrapper query = new QueryWrapper<Notice>();
        query.eq("status", "0");
        query.isNull("receive_time");
        query.orderByAsc("send_time");
        return noticeMapper.selectList(query);
    }


}
