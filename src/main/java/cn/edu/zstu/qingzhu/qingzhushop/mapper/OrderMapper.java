package cn.edu.zstu.qingzhu.qingzhushop.mapper;


import cn.edu.zstu.qingzhu.qingzhushop.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Mapper
@Component
public interface OrderMapper extends BaseMapper<Order> {

    Order selectByOrderId(Integer id);

    Order selectByOrderIdForUpdate(Integer id);

    List<Order> selectByOrderCode(String orderCode);

    int updateByIdSelective(Order order);

    //根据userId获得该用户的所有订单
    List<Order> selectByUserId(Integer id);

    /**
     * 根据userId获得该用户的所有订单
     * 并且携带用户信息和订单详情
     *
     * @param id
     * @return
     */
    List<Order> selectOrderWithDetailByUserId(Integer id);


    Integer selectSumSalesVolume();

    BigDecimal selectSumSales();
}