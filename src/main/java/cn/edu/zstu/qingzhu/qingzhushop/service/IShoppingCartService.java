package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.ShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-07 21:19
 */
public interface IShoppingCartService extends IService<ShoppingCart> {
    //根据shopCartId选择
    public ShoppingCart selectByCartId(Integer cartId);

    //根据userId获得该用户的购物车数据
    public List<ShoppingCart> selectByUserId(Integer userId);

    List<ShoppingCart> listOrderByUserId(Integer id);

    List<ShoppingCart> listShoppingCartUserId(Integer id);

    ShoppingCart listShoppingCartUserIdAndGoodsId(Integer userId, Integer goodsId);
}
