package cn.edu.zstu.qingzhu.qingzhushop.controller;


import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import cn.edu.zstu.qingzhu.qingzhushop.service.IBannerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-11-08
 */
@RestController
@RequestMapping("/banner")
@Api(tags = "轮播图接口")
public class BannerController {
    @Autowired
    private IBannerService iBannerService;
    @RequestMapping(value = "",method = {RequestMethod.GET})
    public ResponseBean list(){
       return ResponseBean.success(iBannerService.list());
    }
}

