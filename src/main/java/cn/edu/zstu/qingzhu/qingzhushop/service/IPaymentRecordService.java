package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.PaymentRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-11-08
 */
public interface IPaymentRecordService extends IService<PaymentRecord> {

}
