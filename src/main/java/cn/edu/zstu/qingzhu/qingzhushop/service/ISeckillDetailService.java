package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Seckill;
import cn.edu.zstu.qingzhu.qingzhushop.entity.SeckillDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * FileName: ISeckillDetailService
 *
 * @Author: LvYibin
 * @Date: 2021/5/17 09:08
 * @Description:
 */
public interface ISeckillDetailService extends IService<SeckillDetail> {
    int saveSeckill(Seckill seckill) throws Exception;
    int saveSeckillDetail(Integer userid,String killid) throws Exception;
}
