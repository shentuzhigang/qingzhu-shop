package cn.edu.zstu.qingzhu.qingzhushop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-26 13:51
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "菜单")
public class Menu {
    @ApiModelProperty(name = "菜单ID", value = "菜单ID")
    private Integer id;
    @ApiModelProperty(name = "菜单匹配", value = "菜单匹")
    private String pattern;
    @ApiModelProperty(name = "路径", value = "路径")
    private String path;
    //
    @ApiModelProperty(name = "类型", value = "类型")
    private String type;
    @ApiModelProperty(name = "组件", value = "组件")
    private String component;
    @ApiModelProperty(name = "名称", value = "名称")
    private String name;
    @ApiModelProperty(name = "元", value = "元")
    private Meta meta;
    @ApiModelProperty(name = "父菜单ID", value = "父菜单ID")
    private Integer parentId;
    @ApiModelProperty(name = "是否可用菜单", value = "是否可用菜单")
    private Boolean enabled;
    @ApiModelProperty(name = "子菜单", value = "子菜单")
    private List<Menu> children;
}
