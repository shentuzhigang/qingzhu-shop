package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-08 19:41
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "商品搜索DTO", description = "")
public class GoodsQueryDTO implements IGenerative<QueryWrapper<Goods>> {
    private QueryModel<QueryWrapper<Goods>> queryModel = new QueryModel<>();
    private Integer categoryId;
    private String name;
    private String id;
    private String stock;
    private String sold;

    @Override
    public QueryWrapper<Goods> generate() {
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        return generate(queryWrapper);
    }

    @Override
    public QueryWrapper<Goods> generate(QueryWrapper<Goods> wrapper) {
        QueryWrapper<Goods> queryWrapper = wrapper;
        queryWrapper.isNotNull("id");
        if (!StringUtils.isEmpty(name)) {
            queryWrapper.and((wrapper1) -> {
                wrapper1.like("goods_name", name)
                        .or()
                        .like("goods_detail", name);
            });
        }
        if (categoryId != null) {
            queryWrapper.eq("goods_category", categoryId);
        }
        if (queryModel != null) {
            queryModel = new QueryModel<>();
        }
        queryModel.generate(queryWrapper);
        return queryWrapper;
    }
}
