package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Log;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-08-02
 */
public interface ILogService extends IService<Log> {

}
