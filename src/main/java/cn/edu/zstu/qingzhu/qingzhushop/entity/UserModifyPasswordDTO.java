package cn.edu.zstu.qingzhu.qingzhushop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-24 23:55
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "用户修改密码DTO")
public class UserModifyPasswordDTO {
    @ApiModelProperty(value = "新密码")
    private String newPassword;
    @ApiModelProperty(value = "旧密码")
    private String oldPassword;
    @ApiModelProperty(value = "确认新密码")
    private String confirmNewPassword;
    @ApiModelProperty(value = "用户名")
    private String phone;
}
