package cn.edu.zstu.qingzhu.qingzhushop.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-05-18 17:33
 */
@Data
public class UserSexCount implements Serializable {
    private Integer man;
    private Integer woman;
    private Integer unknown;
}
