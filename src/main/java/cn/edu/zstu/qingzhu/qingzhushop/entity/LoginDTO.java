package cn.edu.zstu.qingzhu.qingzhushop.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-01-02 22:21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "用户登录DTO", description = "")
public class LoginDTO {
    private String password;
    private String phone;
}
