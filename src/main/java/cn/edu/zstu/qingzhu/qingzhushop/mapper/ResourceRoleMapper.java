package cn.edu.zstu.qingzhu.qingzhushop.mapper;

import cn.edu.zstu.qingzhu.qingzhushop.entity.ResourceRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-25 11:29
 */
@Mapper
@Component
public interface ResourceRoleMapper extends BaseMapper<ResourceRole> {
    List<Integer> getResourceIdsByRoleId(Integer roleId);

    Integer insertResourceRole(@Param("rid") Integer roleId,
                               @Param("resourceIds") Integer[] resourceIds);
}
