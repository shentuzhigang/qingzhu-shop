package cn.edu.zstu.qingzhu.qingzhushop.entity;

import lombok.Data;

import java.util.Date;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-12 13:25
 */
@Data
public class ChatMessage {
    private String from;
    private String to;
    private String content;
    private Date date;
    private String fromNickname;

}
