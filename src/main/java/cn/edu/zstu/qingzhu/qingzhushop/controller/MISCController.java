package cn.edu.zstu.qingzhu.qingzhushop.controller;

import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import cn.edu.zstu.qingzhu.qingzhushop.entity.UserSexCount;
import cn.edu.zstu.qingzhu.qingzhushop.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-05-03 15:03
 */
@RestController
@RequestMapping("/admin/misc")
@CrossOrigin
@Api(tags = "用户统计分析API接口")
@CacheConfig(cacheNames = "useranalysis")
public class MISCController {
    @Autowired
    IUserService iUserService;

    @ApiOperation(value = "用户注册统计",notes = "用户注册统计")
    @Cacheable(value = "UserInfoList",keyGenerator = "simpleKeyGenerator")
    @RequestMapping(value = "/useranalysis",method = RequestMethod.GET)
    public Object userAnalysis(){
        try {
            return ResponseBean.ok("请求成功", iUserService.getUserCount());
        }catch (Exception e){
            return ResponseBean.error("请求失败");
        }
    }

    @ApiOperation(value = "用户性别统计",notes = "用户性别统计")
    @Cacheable(value = "UserInfoList",keyGenerator = "simpleKeyGenerator")
    @RequestMapping(value = "/usersexanalysis",method = RequestMethod.GET)
    public Object userSexAnalysis(){
        try {
            UserSexCount userSexCount = new UserSexCount();
            userSexCount.setMan(iUserService.getSexCount(0));
            userSexCount.setWoman(iUserService.getSexCount(1));
            userSexCount.setUnknown(iUserService.getSexCount(-1));
            return ResponseBean.ok("请求成功",userSexCount);
        }catch (Exception e){
            return ResponseBean.error("请求失败");
        }
    }

    @ApiOperation(value = "用户活跃统计",notes = "用户活跃统计")
    @Cacheable(value = "UserInfoList",keyGenerator = "simpleKeyGenerator")
    @RequestMapping(value = "/useractive",method = RequestMethod.GET)
    public Object userActive(){
        try {
            return ResponseBean.ok("请求成功",iUserService.getUserDateAnalysis());
        }catch (Exception e){
            return ResponseBean.error("请求失败");
        }
    }


    @ApiOperation(value = "用户城市分布统计",notes = "用户城市分布统计")
    @Cacheable(value = "UserInfoList",keyGenerator = "simpleKeyGenerator")
    @RequestMapping(value = "/usercity",method = RequestMethod.GET)
    public Object userCity(){
        try {
            return ResponseBean.ok("请求成功",iUserService.getUserCityAnalysis());
        }catch (Exception e){
            return ResponseBean.error("请求失败");
        }
    }
}
