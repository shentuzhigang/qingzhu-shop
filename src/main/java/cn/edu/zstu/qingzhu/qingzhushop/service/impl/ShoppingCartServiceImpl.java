package cn.edu.zstu.qingzhu.qingzhushop.service.impl;

import cn.edu.zstu.qingzhu.qingzhushop.entity.ShoppingCart;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.ShoppingCartMapper;
import cn.edu.zstu.qingzhu.qingzhushop.service.IShoppingCartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart>
        implements IShoppingCartService {

    @Autowired
    private ShoppingCartMapper shopCartMapper;

    //根据shopCartId选择
    @Override
    public ShoppingCart selectByCartId(Integer cartId) {
        return shopCartMapper.selectByCartId(cartId);
    }

    //根据userId获得该用户的购物车数据
    @Override
    public List<ShoppingCart> selectByUserId(Integer userId) {
        return shopCartMapper.selectByUserId(userId);
    }

    @Override
    public List<ShoppingCart> listOrderByUserId(Integer id) {
        return null;
    }

    @Override
    public List<ShoppingCart> listShoppingCartUserId(Integer id) {
        return null;
    }

    @Override
    public ShoppingCart listShoppingCartUserIdAndGoodsId(Integer userId, Integer goodsId) {
        return shopCartMapper.selectByUserIdAndGoodsId(userId, goodsId);
    }

    public int updateByIdSelective(ShoppingCart shoppingCart) {
        return shopCartMapper.updateByIdSelective(shoppingCart);
    }

}
