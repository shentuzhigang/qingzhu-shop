package cn.edu.zstu.qingzhu.qingzhushop.mapper;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Resource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-03-07 21:59
 */
@Mapper
@Component
public interface ResourceMapper extends BaseMapper<Resource> {

    List<Resource> getAllResource();

    List<Resource> getMenusByUserId(Integer userId);

    List<Resource> getAllResourcesWithChildren(Integer parentId);
}
