package cn.edu.zstu.qingzhu.qingzhushop.service.impl;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Resource;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.ResourceMapper;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.ResourceRoleMapper;
import cn.edu.zstu.qingzhu.qingzhushop.service.IResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-03-08 19:47
 */
@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource>
        implements IResourceService {

    @Autowired
    ResourceMapper resourceMapper;
    @Autowired
    ResourceRoleMapper resourceRoleMapper;

    @Override
    public List<Resource> getAllResource() {

        return resourceMapper.getAllResource();
    }

    @Override
    public List<Resource> getAllResourcesWithChildren() {
        return resourceMapper.getAllResourcesWithChildren(0);
    }

}
