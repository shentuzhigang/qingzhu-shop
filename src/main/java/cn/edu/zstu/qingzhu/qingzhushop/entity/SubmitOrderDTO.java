package cn.edu.zstu.qingzhu.qingzhushop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-01-09 23:37
 */
@Data
@ApiModel(value = "提交订单DTO", description = "提交订单DTO")
public class SubmitOrderDTO implements Serializable {

    @ApiModelProperty(value = "用户ID", example = "2")
    private Integer uid;

    @ApiModelProperty(value = "地址", example = "浙江理工大学")
    private String address;

    @ApiModelProperty(value = "商品ID数组", dataType = "List", example = "[604,605]")
    private String goods;

    @ApiModelProperty(value = "备注", example = "备注.")
    private String message;

    @ApiModelProperty(value = "总金额", example = "1360.50")
    private String amount;
}
