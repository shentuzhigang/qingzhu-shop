package cn.edu.zstu.qingzhu.qingzhushop.controller;

import cn.edu.zstu.qingzhu.qingzhushop.service.IActivityService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-08 15:31
 */
@RestController
@RequestMapping("/activity")
@Api(tags = "活动接口")
public class ActivityController {
    @Autowired
    IActivityService iActivityService;
}
