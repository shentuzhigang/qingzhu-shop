package cn.edu.zstu.qingzhu.qingzhushop.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-05-03 16:59
 */
@Data
public class UserCount implements Serializable {
    private String date;
    private Integer netgainUser;
    private Integer cumulateUser;
}
