package cn.edu.zstu.qingzhu.qingzhushop.controller;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Resource;
import cn.edu.zstu.qingzhu.qingzhushop.service.IResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-25 11:19
 */
@RestController
@RequestMapping("/resource")
@Api(tags = "资源功能接口")
public class ResourceController extends BaseRestController<IResourceService, Resource,Integer> {
    @Autowired
    IResourceService iResourceService;
    /**
     *
     * @return
     */
    @ApiOperation(value = "获取所有资源",notes = "获取所有资源",response = Object.class)
    @RequestMapping(value = "/all",method = {RequestMethod.GET})
    public Object getAllResource(){
        return iResourceService.list();
    }
    /**
     *
     * @return
     */
    @ApiOperation(value = "获取所有资源",notes = "获取所有资源",response = Object.class)
    @RequestMapping(value = "/allWithChildren",method = {RequestMethod.GET})
    public Object getAllResourcesWithChildren(){
        return iResourceService.getAllResourcesWithChildren();
    }


}
