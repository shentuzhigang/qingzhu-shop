package cn.edu.zstu.qingzhu.qingzhushop.service.impl;

import cn.edu.zstu.qingzhu.qingzhushop.entity.OrderDetail;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.OrderDetailMapper;
import cn.edu.zstu.qingzhu.qingzhushop.service.IOrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-11-08
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements IOrderDetailService {
    @Autowired
    OrderDetailMapper orderDetailMapper;

    @Override
    public List<OrderDetail> selectByOrderId(Integer orderId) {
        return orderDetailMapper.getGoodsListByOrderId(orderId);
    }
}
