package cn.edu.zstu.qingzhu.qingzhushop.exception;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-24 23:37
 */
public class RegisterException extends RuntimeException {
    /**
     * 添加一个空参数的构造方法
     */
    public RegisterException() {
        super();
    }

    /**
     * 添加一个带异常信息的构造方法
     * @param name 异常信息
     */
    public RegisterException(String name) {
        super(name);
    }
}

