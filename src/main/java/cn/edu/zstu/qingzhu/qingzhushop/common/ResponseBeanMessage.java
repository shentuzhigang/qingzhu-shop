package cn.edu.zstu.qingzhu.qingzhushop.common;

public interface ResponseBeanMessage {
    String ERROR = "error";

    String SUCCESS = "success";

    String DATA_NOT_EXIST = "未查询到记录！";

    String SAME_CATEGORY_EXIST = "有同级同名的分类！";

    String SAME_LOGIN_NAME_EXIST = "用户名已存在！";

    String LOGIN_NAME_NULL = "请输入登录名！";

    String LOGIN_PASSWORD_NULL = "请输入密码！";

    String LOGIN_VERIFY_CODE_NULL = "请输入验证码！";

    String LOGIN_VERIFY_CODE_ERROR = "验证码错误！";

    String GOODS_NOT_EXIST = "商品不存在！";

    String SHOPPING_CART_ITEM_LIMIT_NUMBER_ERROR = "超出单个商品的最大购买数量！";

    String LOGIN_ERROR = "登录失败！";

    String LOGIN_USER_LOCKED = "用户已被禁止登录！";

    String ORDER_NOT_EXIST_ERROR = "订单不存在！";

    String NULL_ADDRESS_ERROR = "地址不能为空！";

    String ORDER_PRICE_ERROR = "订单价格异常！";

    String ORDER_GENERATE_ERROR = "生成订单异常！";

    String SHOPPING_ITEM_ERROR = "购物车数据异常！";

    String SHOPPING_ITEM_COUNT_ERROR = "库存不足！";

    String ORDER_STATUS_ERROR = "订单状态异常！";

    String OPERATE_ERROR = "操作失败！";

    String DB_ERROR = "database error";

}
