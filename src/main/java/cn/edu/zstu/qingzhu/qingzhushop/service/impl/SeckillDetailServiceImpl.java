package cn.edu.zstu.qingzhu.qingzhushop.service.impl;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Seckill;
import cn.edu.zstu.qingzhu.qingzhushop.entity.SeckillDetail;
import cn.edu.zstu.qingzhu.qingzhushop.entity.User;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.SeckillDetailMapper;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.SeckillMapper;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.UserMapper;
import cn.edu.zstu.qingzhu.qingzhushop.rabbitmq.publisher.SecKillDeadPublisher;
import cn.edu.zstu.qingzhu.qingzhushop.service.ISeckillDetailService;
import cn.edu.zstu.qingzhu.qingzhushop.service.ISeckillService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * FileName: SeckillDetailServiceImpl
 *
 * @Author: LvYibin
 * @Date: 2021/5/17 09:09
 * @Description:
 */
@Service
public class SeckillDetailServiceImpl extends ServiceImpl<SeckillDetailMapper, SeckillDetail> implements ISeckillDetailService {

    @Autowired
    SeckillMapper seckillMapper;
    @Autowired
    SeckillDetailMapper seckillDetailMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    SecKillDeadPublisher secKillDeadPublisher;

    @Override
    @Async
    @Transactional(rollbackFor = Exception.class)
    public int saveSeckill(Seckill seckill) throws  Exception{
        int i = seckillMapper.insert(seckill);
        if (i == 0) {
            throw new Exception("error");
        }
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("onlyseckill",seckill.getOnlyseckill());
        Seckill seckill1 = seckillMapper.selectOne(wrapper);
        try {
            secKillDeadPublisher.sendMsg(seckill1);
        }catch (Exception e){
            return 0;
        }
        return 1;
    }

    @Override
    @Async
    @Transactional(rollbackFor = Exception.class)
    public int saveSeckillDetail(Integer userid, String killid) throws Exception{
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("onlyseckill",killid);
        Seckill seckill = seckillMapper.selectOne(wrapper);
        User user = userMapper.selectById(userid);
        seckill.setTotal(seckill.getTotal()-1);
        int i = 0;
        if (seckill!=null && user!=null){
            SeckillDetail seckillDetail = new SeckillDetail();
            seckillDetail.setAddress(user.getAddress());
            seckillDetail.setAmount(String.valueOf(seckill.getAmount()));
            seckillDetail.setCustomerId(userid);
            seckillDetail.setEmployeeId(0);
            seckillDetail.setMessage("抢购成功");
            seckillDetail.setStatus("未付款");
            seckillDetail.setTotalnumber(1);
            seckillDetail.setSeckillId(seckill.getId());
             i = seckillDetailMapper.insert(seckillDetail);
             seckillMapper.updateById(seckill);
        }else {
            throw new Exception("error");
        }
        return i;
    }
}
