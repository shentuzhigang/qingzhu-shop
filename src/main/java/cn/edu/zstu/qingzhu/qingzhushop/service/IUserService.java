package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.AuthenticationUser;
import cn.edu.zstu.qingzhu.qingzhushop.entity.User;
import cn.edu.zstu.qingzhu.qingzhushop.entity.UserCount;
import cn.edu.zstu.qingzhu.qingzhushop.entity.VueRouteConfig;
import cn.edu.zstu.qingzhu.qingzhushop.exception.RegisterException;
import com.baomidou.mybatisplus.extension.service.IService;
import javafx.util.Pair;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-10-17 18:34
 */
public interface IUserService extends UserDetailsService, IService<User> {
    AuthenticationUser getCurrentAuthenticationUser();

    //根据用户名查询用户
    List<User> selectByUserName(String userName);

    //根据userId查找
    User selectByUserId(Integer id);

    int updateUserface(MultipartFile file, Integer id);

    List<User> getAllUserWithRoles();

    List<User> getAllUserWithRoles(String keyword);

    List<User> getAllUserWithRoles(Integer userId, String keyword);

    List<VueRouteConfig> getVueRouteConfig(AuthenticationUser authenticationUser);

    boolean updateUserRole(Integer userId, Integer[] roleIds);

    List<UserCount> getUserCount();

    Integer getSexCount(Integer query);

    Map<String, Integer> getUserDateAnalysis();

    List<Pair<String, Integer>> getUserCityAnalysis();

    boolean lockUser(Integer id, Integer lockStatus);

    User registerUser(String username, String password) throws RegisterException;

    boolean modifyPassword(String username, String oldPassword, String newPassword);

    List<User> selectByUserPhone(String phone);
}
