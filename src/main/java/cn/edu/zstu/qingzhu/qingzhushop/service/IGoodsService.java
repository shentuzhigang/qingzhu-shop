package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Goods;
import cn.edu.zstu.qingzhu.qingzhushop.entity.GoodsSearchDTO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-07 21:32
 */
public interface IGoodsService extends IService<Goods> {
    public Goods selectByGoodsId(Integer id);

    public List<Goods> selectByGoodsName(String goods_Name);

    //根据活动id查询所有属于这个互动的商品
    public List<Goods> selectByActivityId(Integer activityId);

    public int updateByIdSelective(Goods Goods);

    public int addGood2Activity(Integer goodIds, Integer activityId);

    public int removeGoodsFromActivity(Integer goodsId, Integer activityId);


    Page<Goods> searchGoods(GoodsSearchDTO goodsSearchDTO);

    Boolean updateGoodsPicURL(Integer id, String url);

    int addGoodsToActivity(Integer goodsId, Integer activityId);

    List<Integer> listIdsByGoodsCategoryId(Integer id);

    List<Integer> listAllGoodsId();
}
