package cn.edu.zstu.qingzhu.qingzhushop.mapper;

import cn.edu.zstu.qingzhu.qingzhushop.entity.GoodsCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-11-08
 */
@Mapper
@Component
public interface GoodsCategoryMapper extends BaseMapper<GoodsCategory> {

}
