package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-23 11:25
 */
public interface IRoleService extends IService<Role> {
    List<Integer> getResourceIdsByRoleId(Integer roleId);

    List<Integer> updateResourceRole(Integer roleId, Integer[] resourceIds);
}
