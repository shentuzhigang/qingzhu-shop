package cn.edu.zstu.qingzhu.qingzhushop.controller.admin;


import cn.edu.zstu.qingzhu.qingzhushop.controller.BaseRestController;
import cn.edu.zstu.qingzhu.qingzhushop.entity.Activity;
import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import cn.edu.zstu.qingzhu.qingzhushop.service.IActivityService;
import cn.edu.zstu.qingzhu.qingzhushop.service.IGoodsService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/activity")
@Api(tags = "活动管理接口")
@CrossOrigin(origins={"*"}, methods={RequestMethod.GET, RequestMethod.POST})
public class ActivityManagerController extends BaseRestController<IActivityService,Activity,Integer> {

    @Autowired
    IActivityService iActivityService;

    @Autowired
    IGoodsService iGoodsService;

    //获取所有活动
    @GetMapping("/list")
    @ResponseBody
    public ResponseBean getAllActivities() {
        List<Activity> activitiesList = iActivityService.list();
        return ResponseBean.success("activities", activitiesList);
    }
    //查找活动
//    @PostMapping("/searchActivity")
//    @ResponseBody
//    public ResponseBean searchActivity(@RequestParam("activityName") String activityName){
//        List<Activity> activitiesList = iActivityService.selectByActivityName(activityName);
//        return ResponseBean.success("activities",activitiesList);
//    }
//
//    //根据activityId获取所有商品
//    @GetMapping("/goodsOfActivity")
//    @ResponseBody
//    public ResponseBean goodsOfActivity(@RequestParam("activityId") Integer activityId){
//        List<Goods> goodsList = iGoodsService.selectByActivityId(activityId);
//        return ResponseBean.success("查询成功", goodsList);
//    }
//    //将商品添加进某个活动
//
//    @PostMapping("/addGoodsToActivity")
//    @ResponseBody
//    public ResponseBean addGoodsToActivity(Integer[] goodsIds, Integer activityId) {
//        if(iActivityService.getById(activityId)==null){
//            return ResponseBean.error("活动不存在");
//        }
//        for (Integer id: goodsIds) {
//            if(iGoodsService.addGoodsToActivity(id,activityId)==0){
//                return ResponseBean.error();
//            }
//        }
//        return ResponseBean.success();
//    }
//
//    //将商品从某个活动取消
//
//    @PostMapping("/removeGoodsFromActivity")
//    @ResponseBody
//    public ResponseBean removeGoodsFromActivity(@RequestParam("goodsId") Integer goodsId,
//                                               @RequestParam("activityId") Integer activityId) {
//        if(iGoodsService.removeGoodsFromActivity(goodsId,activityId)!=0){
//            return ResponseBean.success();
//        }else{
//            return ResponseBean.error();
//        }
//    }
}