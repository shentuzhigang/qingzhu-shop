package cn.edu.zstu.qingzhu.qingzhushop.mapper;


import cn.edu.zstu.qingzhu.qingzhushop.entity.Activity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Mapper
@Component
public interface ActivityMapper extends BaseMapper<Activity> {

    Activity selectByActivityId(Integer id);

    ArrayList<Activity> selectByActivityName(String activityName);

    int updateByIdSelective(Activity activity);
}