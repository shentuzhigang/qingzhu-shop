package cn.edu.zstu.qingzhu.qingzhushop.config;

import cn.ucloud.ufile.api.object.ObjectConfig;
import cn.ucloud.ufile.auth.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-17 15:15
 */
@Configuration
public class CustomUCloudUFileConfig {
    @Value("${ucloud.ufile.public-key}")
    private String publicKey;

    @Value("${ucloud.ufile.private-key}")
    private String privateKey;

    @Value("${ucloud.ufile.bucket-name}")
    private String bucketName;

    @Value("${ucloud.ufile.region}")
    private String region;

    @Value("${ucloud.ufile.proxy-suffix}")
    private String proxySuffix;

    @Value("${ucloud.ufile.expires}")
    private Integer expires;

    @Value("${ucloud.ufile.log.tag}")
    private String tag;


    /**
     * 本地Bucket相关API的签名器（账号在ucloud 的API 公私钥，不能使用token）
     * 如果只用到了文件操作，不需要配置下面的bucket 操作公私钥
     */
    @Bean
    public BucketAuthorization bucketAuthorization() {
        return new UfileBucketLocalAuthorization(
                "UcloudPublicKey",
                "UcloudPrivateKey");
    }

    /**
     * 本地Object相关API的签名器
     * 请修改下面的公私钥
     */
    @Bean
    public UfileObjectLocalAuthorization ufileObjectLocalAuthorization() {
        return new UfileObjectLocalAuthorization(
                publicKey,
                privateKey);
    }

    /**
     * 远程Object相关API的签名器
     */
    @Bean
    public UfileObjectRemoteAuthorization ufileObjectRemoteAuthorization() {
        return new UfileObjectRemoteAuthorization("UcloudPublicKey",
                new ObjectRemoteAuthorization.ApiConfig(
                        "http://your_domain/applyAuth",
                        "http://your_domain/applyPrivateUrlAuth"
                ));
    }

    @Bean
    public ObjectConfig objectConfig() {
        return new ObjectConfig(
                region,
                proxySuffix);
    }
}
