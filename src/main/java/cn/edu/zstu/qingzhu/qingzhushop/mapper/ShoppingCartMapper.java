package cn.edu.zstu.qingzhu.qingzhushop.mapper;


import cn.edu.zstu.qingzhu.qingzhushop.entity.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Mapper
@Component
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {

    //根据shopCartId选择
    ShoppingCart selectByCartId(Integer id);

    //根据userId获得该用户的购物车数据
    ArrayList<ShoppingCart> selectByUserId(Integer uid);

    @Select("select * from shopping_cart where uid=#{userId} and goods_id=#{goodsId}")
    ShoppingCart selectByUserIdAndGoodsId(Integer userId, Integer goodsId);

    int updateByIdSelective(ShoppingCart shoppingCart);
}