package cn.edu.zstu.qingzhu.qingzhushop.mapper;

import cn.edu.zstu.qingzhu.qingzhushop.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-25 11:33
 */
@Mapper
@Component
public interface UserRoleMapper extends BaseMapper<UserRole> {
    Integer insertUserRole(@Param("uid") Integer userId,
                           @Param("roleIds") Integer roleIds);
}
