package cn.edu.zstu.qingzhu.qingzhushop.controller.admin;


import cn.edu.zstu.qingzhu.qingzhushop.controller.BaseRestController;
import cn.edu.zstu.qingzhu.qingzhushop.entity.Goods;
import cn.edu.zstu.qingzhu.qingzhushop.entity.Order;
import cn.edu.zstu.qingzhu.qingzhushop.entity.OrderDetail;
import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import cn.edu.zstu.qingzhu.qingzhushop.service.IGoodsService;
import cn.edu.zstu.qingzhu.qingzhushop.service.IOrderDetailService;
import cn.edu.zstu.qingzhu.qingzhushop.service.IOrderService;
import cn.edu.zstu.qingzhu.qingzhushop.service.IShoppingCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/admin/order")
@Api(tags = "订单管理接口")
@CrossOrigin(origins={"*"}, methods={RequestMethod.GET, RequestMethod.POST})
public class OrderManagerController  extends BaseRestController<IOrderService,Order,Integer> {

    @Autowired
    IOrderService iOrderService;

    @Autowired
    IShoppingCartService iShoppingCartService;

    @Autowired
    IGoodsService iGoodsService;

    @Autowired
    IOrderDetailService iOrderDetailService;

    /**
     * 获取所有订单
     * @return
     */
    @CrossOrigin(origins={"*"}, methods={RequestMethod.GET, RequestMethod.POST})
    @GetMapping("/list")
    @ResponseBody
    public ResponseBean listOrders() {
        List<Order> ordersList = iOrderService.list();
        //将订单的所有购物网条目取出
        orderListToShoppingCart(ordersList);
        return ResponseBean.success("orders", ordersList);
    }

    /**
     * 增加订单
     * @param order
     * @return
     */
    @CrossOrigin(origins={"*"}, methods={RequestMethod.GET, RequestMethod.POST})
    @PostMapping("/save")
    @ResponseBody
    public ResponseBean addOrder(@RequestBody Order order){
        //order.setOrderCode(String.valueOf(System.currentTimeMillis()));
        order.setId(null);
        if(iOrderService.save(order)){
            List<Order> ordersList = iOrderService.list();
            return ResponseBean.success("orders", ordersList);
        }else{
            return ResponseBean.error();
        }
    }

    /**
     * 删除订单
     * @param orderId
     * @return
     */
    @CrossOrigin(origins={"*"}, methods={RequestMethod.GET, RequestMethod.POST})
    @PostMapping("/delete")
    @ResponseBody
    public ResponseBean deleteOrder(@RequestParam("id") Integer orderId){
        if(iOrderService.removeById(orderId)){
            List<Order> ordersList = iOrderService.list();
            return ResponseBean.success("orders", ordersList);
        }else{
            return ResponseBean.error();
        }
    }

    /**
     * 修改订单
     * @param order
     * @return
     */
    @CrossOrigin(origins={"*"}, methods={RequestMethod.GET, RequestMethod.POST})
    @PostMapping("/update")
    @ResponseBody
    public ResponseBean updateOrder(@RequestBody Order order){
        if(iOrderService.updateByIdSelective(order)!=0){
            List<Order> ordersList = iOrderService.list();
            return ResponseBean.success("orders", ordersList);
        }else{
            return ResponseBean.error();
        }
    }

    /**
     * 根据订单Id查订单
     * @param orderId
     * @return
     */
    @CrossOrigin(origins={"*"}, methods={RequestMethod.GET, RequestMethod.POST})
    @GetMapping("/search")
    @ResponseBody
    public ResponseBean searchOrderById(@RequestParam("orderId") Integer orderId){
        Order order = iOrderService.selectByOrderId(orderId);
        //将订单的所有购物网条目取出
        orderToShoppingCart(order);
        return ResponseBean.success("order",order);
    }

    /**
     * 根据userId获得该用户的所有订单
     * @param userId
     * @return
     */
    @CrossOrigin(origins={"*"}, methods={RequestMethod.GET, RequestMethod.POST})
    @GetMapping("/orderOfUser")
    @ResponseBody
    public ResponseBean orderOfUser(@RequestParam("userId") Integer userId){
        List<Order> ordersList = iOrderService.selectByUserId(userId);
        //将订单的所有购物网条目取出
        orderListToShoppingCart(ordersList);
        return ResponseBean.success("orders",ordersList);
    }

    /**
     * 订单数查询
     * @return
     */
    @CrossOrigin(origins={"*"}, methods={RequestMethod.GET})
    @ApiOperation(value = "订单数",notes = "获取订单数")
    @GetMapping("/sum")
    @ResponseBody
    public ResponseBean sum(){
        return ResponseBean.success("订单总数",iOrderService.count());
    }


    /**
     * 总销售量
     * @return
     */
    @CrossOrigin(origins={"*"}, methods={RequestMethod.GET})
    @ApiOperation(value = "总销售量",notes = "获取总销售量")
    @GetMapping("/volume")
    @ResponseBody
    public ResponseBean volume(){
        return ResponseBean.success("总销售量",iOrderService.sumSalesVolume());
    }


    /**
     * 总销售额
     * @return
     */
    @CrossOrigin(origins={"*"}, methods={RequestMethod.GET})
    @ApiOperation(value = "总销售额",notes = "获取总销售额")
    @GetMapping("/sales")
    @ResponseBody
    public ResponseBean sales(){
        return ResponseBean.success("总销售额",iOrderService.sumSales());
    }

    /**
     *
     * @param ordersList
     */
    private void orderListToShoppingCart(List<Order> ordersList){
        for (Order order:ordersList) {
            orderToShoppingCart(order);
        }
    }

    /**
     *
     * @param order
     */
    private void orderToShoppingCart(Order order){
        List<Goods> cartList = new ArrayList<>();
        //String[] cartIds = order.getDetail().split(",");
        List<OrderDetail> orderDetails = iOrderDetailService.selectByOrderId(order.getId());
        for(int i=0;i<orderDetails.size();++i){
            //ShoppingCart cart = iShoppingCartService.selectByCartId(Integer.parseInt(cartIds[i]));
            System.out.println(orderDetails.get(i).getGoodsId());
            cartList.add(iGoodsService.selectByGoodsId(orderDetails.get(i).getGoodsId()));
        }
        order.setCartList(cartList);
    }
}
