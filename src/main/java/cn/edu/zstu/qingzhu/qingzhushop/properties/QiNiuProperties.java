package cn.edu.zstu.qingzhu.qingzhushop.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-29 23:58
 */
@Data
@Component
@ConfigurationProperties(prefix = "qiniu", ignoreUnknownFields = true)
public class QiNiuProperties {

    private String accessKey ;

    private String secretKey ;

    private String region;

    private String bucketName ;

    private String domain;
}
