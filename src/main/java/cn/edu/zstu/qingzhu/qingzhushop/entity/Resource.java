package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-03-07 22:03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "资源")
public class Resource {
    //ID
    @ApiModelProperty(name = "资源ID", value = "资源ID")
    @TableId(value = "id", type = IdType.AUTO)
    private int id;
    //资源名
    @ApiModelProperty(name = "资源名", value = "资源名")
    private String name;
    //资源名
    @ApiModelProperty(name = "资源匹配", value = "资源匹配")
    private String pattern;
    //
    @ApiModelProperty(name = "路径", value = "路径")
    private String path;
    //
    @ApiModelProperty(name = "类型", value = "类型")
    private String type;
    //
    @ApiModelProperty(name = "组件", value = "组件")
    private String component;
    //
    @ApiModelProperty(name = "图标", value = "图标")
    private String icon;
    //
    @ApiModelProperty(name = "保持", value = "保持")
    private Boolean keepAlive;
    //
    @ApiModelProperty(name = "是否需要授权", value = "是否需要授权")
    private Boolean requireAuth;
    //
    @ApiModelProperty(name = "父菜单ID", value = "父菜单ID")
    private Integer parentId;
    //
    @ApiModelProperty(name = "是否可用菜单", value = "是否可用菜单")
    private Boolean enabled;
    //可授权角色列表
    @ApiModelProperty(value = "可授权角色列表")
    @TableField(exist = false)
    private List<Role> roles;
    //子资源列表
    @ApiModelProperty(value = "子资源列表")
    @TableField(exist = false)
    private List<Resource> children;

}
