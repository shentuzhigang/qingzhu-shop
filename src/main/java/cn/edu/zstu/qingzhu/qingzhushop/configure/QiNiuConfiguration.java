package cn.edu.zstu.qingzhu.qingzhushop.configure;

import cn.edu.zstu.qingzhu.qingzhushop.properties.QiNiuProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-30 00:15
 */
@EnableConfigurationProperties(QiNiuProperties.class)
public class QiNiuConfiguration {
}
