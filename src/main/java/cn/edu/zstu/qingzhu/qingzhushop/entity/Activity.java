package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class Activity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String activityName;
    private String activityDetail;
    private Date startTime;
    private Date endTime;
    private String str_startTime;
    private String str_endTime;
    private String goods;


}
