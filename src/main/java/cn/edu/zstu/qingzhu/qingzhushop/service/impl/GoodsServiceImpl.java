package cn.edu.zstu.qingzhu.qingzhushop.service.impl;

import cn.edu.zstu.qingzhu.qingzhushop.common.Constants;
import cn.edu.zstu.qingzhu.qingzhushop.entity.Goods;
import cn.edu.zstu.qingzhu.qingzhushop.entity.GoodsSearchDTO;
import cn.edu.zstu.qingzhu.qingzhushop.entity.QueryModel;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.GoodsMapper;
import cn.edu.zstu.qingzhu.qingzhushop.service.IGoodsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods>
        implements IGoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public Goods selectByGoodsId(Integer id) {
        return goodsMapper.selectByGoodsId(id);
    }

    @Override
    public List<Goods> selectByGoodsName(String goods_Name) {
        return goodsMapper.selectByGoodsName(goods_Name);
    }

    @Override
    public int updateByIdSelective(Goods goods) {
        UpdateWrapper<Goods> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", goods.getId());
        updateWrapper.set("picURL", goods.getPicURL());
        return goodsMapper.updateById(goods);
    }

    //根据活动id查询所有属于这个互动的商品
    @Override
    public List<Goods> selectByActivityId(Integer activityId) {
        return goodsMapper.selectByActivityId(activityId);
    }

    @Override
    public int addGood2Activity(Integer goodIds, Integer activityId) {
        return goodsMapper.addGood2Activity(goodIds, activityId);
    }

    @Override
    public int removeGoodsFromActivity(Integer goodsId, Integer activityId) {
        return goodsMapper.rmvGoodFromAct(goodsId, activityId);
    }

    @Override
    public Page<Goods> searchGoods(GoodsSearchDTO goodsSearchDTO) {
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNotNull("id");


        if (!StringUtils.isEmpty(goodsSearchDTO.getKeyword())) {
            queryWrapper.like("goods_name", goodsSearchDTO.getKeyword());
            queryWrapper.like("goods_detail", goodsSearchDTO.getKeyword());
        }
        if (goodsSearchDTO.getGoodsCategoryId() != null) {
            queryWrapper.eq("goods_category", goodsSearchDTO.getGoodsCategoryId());
        }
        QueryModel queryModel = goodsSearchDTO.getQueryModel();
        if (!StringUtils.isEmpty(queryModel.getSortName()) && !StringUtils.isEmpty(queryModel.getSortOrder())) {

            if ("ASC".equals(queryModel.getSortOrder())) {
                queryWrapper.orderByAsc(queryModel.getSortName());
            } else if ("DESC".equals(queryModel.getSortOrder())) {
                queryWrapper.orderByDesc(queryModel.getSortName());
            } else {
                queryWrapper.orderByAsc(queryModel.getSortName());
            }
        } else {
            queryWrapper.orderByAsc(queryModel.getSortName());
        }


        if (StringUtils.isEmpty(queryModel.getCurrent())) {
            queryModel.setCurrent(1);
        }
        if (StringUtils.isEmpty(queryModel.getShowCount()) || queryModel.getShowCount() > 100) {
            queryModel.setShowCount(Constants.GOODS_SEARCH_PAGE_LIMIT);
        }

        return goodsMapper.selectPage(new Page<>(goodsSearchDTO.getQueryModel().getCurrent(),
                goodsSearchDTO.getQueryModel().getShowCount()), queryWrapper);
    }

    @Override
    public Boolean updateGoodsPicURL(Integer id, String url) {
        try {
            UpdateWrapper<Goods> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("id", id);
            updateWrapper.set("picURL", url);
            Goods goods = new Goods();
            goods.setId(id);
            goods.setPicURL(url);
            return goodsMapper.updateById(goods) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int addGoodsToActivity(Integer goodsId, Integer activityId) {
        return goodsMapper.addGood2Activity(goodsId, activityId);
    }

    @Override
    public List<Integer> listIdsByGoodsCategoryId(Integer id) {
        return goodsMapper.selectIdListByGoodsCategoryId(id);
    }

    @Override
    public List<Integer> listAllGoodsId() {
        return goodsMapper.selectAllGoodsId();
    }
}
