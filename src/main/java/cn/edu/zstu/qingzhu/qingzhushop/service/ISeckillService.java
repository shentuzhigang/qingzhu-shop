package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Goods;
import cn.edu.zstu.qingzhu.qingzhushop.entity.Seckill;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * FileName: ISeckillService
 *
 * @Author: LvYibin
 * @Date: 2021/5/17 09:05
 * @Description:
 */
public interface ISeckillService extends IService<Seckill> {
    String handout(Seckill seckill) throws Exception;

    int rob(Integer userId,String killId) throws Exception;
}
