package cn.edu.zstu.qingzhu.qingzhushop.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-24 23:13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "用户注册DTO", description = "")
public class RegisterDTO implements Serializable {
    private String password;
    private String phone;
}
