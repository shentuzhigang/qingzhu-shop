package cn.edu.zstu.qingzhu.qingzhushop.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-30 15:12
 */
@Data
@Component
@ConfigurationProperties(prefix = "ucloud", ignoreUnknownFields = true)
public class UCloudProperties {
    private UFile ufile = new UFile();
    @Data
    private class UFile{

        private String publicKey ;

        private String privateKey ;

        private String region;

        private String bucketName ;

        private String proxySuffix;

        private Long expires;

    }
}
