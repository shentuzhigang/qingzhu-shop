package cn.edu.zstu.qingzhu.qingzhushop.handler;

import cn.edu.zstu.qingzhu.qingzhushop.entity.AuthenticationUser;
import cn.edu.zstu.qingzhu.qingzhushop.entity.Log;
import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import cn.edu.zstu.qingzhu.qingzhushop.service.ILogService;
import cn.edu.zstu.qingzhu.qingzhushop.util.HttpServletUtil;
import cn.edu.zstu.qingzhu.qingzhushop.util.IPUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-03-21 13:10
 */

@Component
public class CustomSavedRequestAwareAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    ILogService iLogService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication)
            throws ServletException, IOException {
        RequestCache requestCache = new HttpSessionRequestCache();
        SavedRequest savedRequest = requestCache.getRequest(httpServletRequest, httpServletResponse);
        String ip = IPUtil.getIPAddress(httpServletRequest);
        AuthenticationUser user = (AuthenticationUser) authentication.getPrincipal();
        logSuccess(ip, user);
        MediaType acceptMediaType = HttpServletUtil.getRequestPrimaryAcceptMediaType(httpServletRequest);
        if (MediaType.TEXT_HTML.compareTo(acceptMediaType) == 0) {
            if (savedRequest != null) {
                //url = savedRequest.getRedirectUrl();
                super.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);
            } else {
                getRedirectStrategy().sendRedirect(httpServletRequest, httpServletResponse, "/");
            }
        } else if (MediaType.APPLICATION_JSON.compareTo(acceptMediaType) == 0) {
            ResponseBean ok = ResponseBean.ok("登录成功!", user);
            HttpServletUtil.writeObjectToResponse(httpServletResponse, ok);
        } else if (MediaType.ALL.compareTo(acceptMediaType) == 0) {
            ResponseBean ok = ResponseBean.ok("登录成功!", user);
            HttpServletUtil.writeObjectToResponse(httpServletResponse, ok);
        }
    }

    private void logSuccess(String ip, AuthenticationUser user) {
        //登录日志
        Log loginLog = new Log();
        loginLog.setOperation("登录");
        loginLog.setType("login");
        loginLog.setIp(ip);
        loginLog.setOperator(user.getUsername());
        loginLog.setTime(LocalDateTime.now());
        loginLog.setStatus("success");
        loginLog.setRemark("登录成功!");
        iLogService.save(loginLog);
    }
}