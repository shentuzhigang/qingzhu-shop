package cn.edu.zstu.qingzhu.qingzhushop.rabbitmq.consumer;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Order;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.OrderMapper;
import cn.edu.zstu.qingzhu.qingzhushop.rabbitmq.DeadInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

/**
 * FileName: DeadConsumer
 *
 * @Author: LvYibin
 * @Date: 2020/7/21 14:55
 * @Description:
 */
@Component
@RabbitListener(queues = "queue2",containerFactory = "singleListenerContainer")
public class DeadConsumer {
    private static final Logger log= LoggerFactory.getLogger(DeadConsumer.class);
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    OrderMapper orderMapper;
    /**
     * 监听并消费队列中的消息-在这里采用单一容器工厂实例即可
     */
    @RabbitHandler
    public void consumeMsg(@Payload byte[] msg){
        try {
            //String message=new String(msg, StandardCharsets.UTF_8);
//            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
//            List<DeadInfo> list = objectMapper.readValue(new String(msg, StandardCharsets.UTF_8),
//                    new TypeReference<List<DeadInfo>>(){/**/});
            Integer id = new Integer(new String(msg,StandardCharsets.UTF_8));
            Order order = orderMapper.selectByOrderIdForUpdate(id);
            if (order.getStatus().equals("未付款")){
                order.setStatus("已超时");
                orderMapper.updateByIdSelective(order);
            }
//            DeadInfo list = objectMapper.readValue(new String(msg,StandardCharsets.UTF_8),DeadInfo.class);
            log.info("死信消息模型-消费者-消费消息-订单编号：{}",id);

        }catch (Exception e){
            log.error("死信消息模型-消费者-发生异常：",e.fillInStackTrace());
        }
    }
}
