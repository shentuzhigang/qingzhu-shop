package cn.edu.zstu.qingzhu.qingzhushop.entity;

import cn.edu.zstu.qingzhu.qingzhushop.common.ResponseBeanMessage;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-02-10 12:51
 */
@Data
public class ResponseBean implements Serializable {
    private Integer status;

    private String msg;

    private Object data;

    private ResponseBean() {
    }

    public ResponseBean(Integer status) {
        this.status = status;
    }

    public ResponseBean(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    private ResponseBean(Integer status, String msg, Object obj) {
        this.status = status;
        this.msg = msg;
        this.data = obj;
    }

    public static ResponseBean success() {
        return success(ResponseBeanMessage.SUCCESS);
    }

    public static ResponseBean success(String msg) {
        return success(msg, null);
    }

    public static ResponseBean success(Object obj) {
        return success(ResponseBeanMessage.SUCCESS, obj);
    }

    public static ResponseBean success(String msg, Object obj) {
        return new ResponseBean(200, msg, obj);
    }

    public static ResponseBean ok(String msg) {
        return success(msg);
    }

    public static ResponseBean ok(String msg, Object obj) {
        return success(msg, obj);
    }

    public static ResponseBean error() {
        return error(ResponseBeanMessage.ERROR);
    }

    public static ResponseBean error(String msg) {
        return error(msg, null);
    }

    public static ResponseBean error(Object obj) {
        return new ResponseBean(500, ResponseBeanMessage.ERROR, obj);
    }

    public static ResponseBean error(String msg, Object obj) {
        return new ResponseBean(500, msg, obj);
    }

    @Data
    @Accessors(chain = true)
    public static class Builder {
        private Integer status;

        private String msg;

        private Object data;

        public ResponseBean build() {
            return new ResponseBean(this.status, this.msg, this.data);
        }
    }
}