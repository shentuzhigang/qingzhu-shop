package cn.edu.zstu.qingzhu.qingzhushop.controller;

import cn.edu.zstu.qingzhu.qingzhushop.common.Constants;
import cn.edu.zstu.qingzhu.qingzhushop.entity.*;
import cn.edu.zstu.qingzhu.qingzhushop.service.IGoodsCategoryService;
import cn.edu.zstu.qingzhu.qingzhushop.service.IGoodsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-08 15:32
 */
@Slf4j
@RestController
@RequestMapping("/goods")
@Api(tags = "商品接口")
public class GoodsController {
    @Autowired
    private IGoodsService iGoodsService;
    @Autowired
    private IGoodsCategoryService iGoodsCategoryService;

    /**
     *
     * @param goodsSearchDTO
     * @return
     */
    @ApiOperation(value = "搜索商品",notes = "搜索商品",response = ResponseBean.class)
    @RequestMapping(value = {"/search"},method = {RequestMethod.GET})
    public ResponseBean searchPage(@ApiParam(name = "queryModel",value = "查询模型") GoodsSearchDTO goodsSearchDTO) {
        log.debug(String.valueOf(goodsSearchDTO));
        QueryModel queryModel = goodsSearchDTO.getQueryModel();
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isEmpty(queryModel.getCurrent())) {
            queryModel.setCurrent(1);
        }
        if(StringUtils.isEmpty(queryModel.getShowCount()) || queryModel.getShowCount() > 100){
            queryModel.setShowCount(Constants.GOODS_SEARCH_PAGE_LIMIT);
        }

        return ResponseBean.success("查询成功",iGoodsService.searchGoods(goodsSearchDTO));
    }

    /**
     *
     * @param goodsId
     * @return
     */
    @ApiOperation(value = "获取商品详细",notes = "获取商品详细",response = ResponseBean.class)
    @RequestMapping(value = "/detail/{goodsId}",method = {RequestMethod.GET})
    public ResponseBean detailPage(@PathVariable("goodsId") Long goodsId) {
        if (goodsId < 1) {
            return ResponseBean.error("内部错误");
        }
        Goods goods = iGoodsService.getById(goodsId);
        if (goods == null) {
            return ResponseBean.error("错误");
        }
        return ResponseBean.success("查询成功", goods);
    }

    /**
     *
     * @return
     */
    @ApiOperation(value = "根据分类获取商品id",notes = "根据分类获取商品id",response = ResponseBean.class)
    @RequestMapping(value = "/getIdsByClass",method = {RequestMethod.GET})
    public ResponseBean getIdsByClass(@RequestParam(name = "className") String className){
        QueryWrapper<GoodsCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("category_name",className);
        GoodsCategory one = iGoodsCategoryService.getOne(queryWrapper);
        if (one==null) {
            return ResponseBean.error("商品分类不存在");
        }
        List<Integer> ids = iGoodsService.listIdsByGoodsCategoryId(one.getId());
        return ResponseBean.success(ids);
    }

}
