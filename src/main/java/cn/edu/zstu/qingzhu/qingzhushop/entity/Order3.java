package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-12-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Order对象", description = "")
@TableName(value = "`order`")
public class Order3 implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String amount;

    private String status;

    private String address;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private List<ShoppingCart> cartList = new ArrayList<>();//订单对应的购物车数据

    private List<Goods> detail;  //detail包含所有的购物车id

}
