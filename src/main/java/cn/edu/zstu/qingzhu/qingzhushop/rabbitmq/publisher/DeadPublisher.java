package cn.edu.zstu.qingzhu.qingzhushop.rabbitmq.publisher;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

/**
 * FileName: DeadPublisher
 *
 * @Author: LvYibin
 * @Date: 2020/7/21 14:55
 * @Description:
 */
@Component
public class DeadPublisher {
    private static final Logger log= LoggerFactory.getLogger(DeadPublisher.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;




    /**
     * 发送消息
     * @param message
     */
    public void sendMsg(String message){

        try {
            //rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
            Message msg = MessageBuilder.withBody(message.getBytes(StandardCharsets.UTF_8)).setDeliveryMode(
                    MessageDeliveryMode.PERSISTENT).build();
            rabbitTemplate.convertAndSend("topic:exchange1", "routing:key:2", msg, new MessagePostProcessor() {
                @Override
                public Message postProcessMessage(Message message) throws AmqpException {
                    MessageProperties messageProperties = message.getMessageProperties();
                    //messageProperties.setHeader(AbstractJavaTypeMapper.DEFAULT_CONTENT_CLASSID_FIELD_NAME, DeadInfo.class);
                    messageProperties.setExpiration(String.valueOf(10000));
                    return message;
                }

            });
            log.info("死信消息模型-生产者-发送消息-订单编号: {} ",message);
        }catch (Exception e){
            log.error("死信消息模型-生产者-发送消息发生异常：{} ",message,e.fillInStackTrace());
        }

    }
}
