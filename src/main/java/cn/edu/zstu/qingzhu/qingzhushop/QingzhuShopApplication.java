package cn.edu.zstu.qingzhu.qingzhushop;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.session.config.annotation.web.http.EnableSpringHttpSession;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = "cn.edu.zstu.qingzhu.qingzhushop.mapper")
@EnableCaching
@EnableScheduling
@EnableWebSocket
@EnableRedisHttpSession
@EnableSpringHttpSession
public class QingzhuShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(QingzhuShopApplication.class, args);
    }

}
