package cn.edu.zstu.qingzhu.qingzhushop.controller.admin;

import cn.edu.zstu.qingzhu.qingzhushop.controller.BaseRestController;
import cn.edu.zstu.qingzhu.qingzhushop.entity.Banner;
import cn.edu.zstu.qingzhu.qingzhushop.service.IBannerService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-22 21:01
 */
@RestController
@RequestMapping("/admin/banner")
@Api(tags = "轮播图管理接口")
public class BannerManagerController  extends BaseRestController<IBannerService, Banner,Integer> {
}
