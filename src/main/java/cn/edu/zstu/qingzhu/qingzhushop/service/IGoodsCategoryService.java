package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.GoodsCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-11-08
 */
public interface IGoodsCategoryService extends IService<GoodsCategory> {
    List<GoodsCategory> listWithTree();
}
