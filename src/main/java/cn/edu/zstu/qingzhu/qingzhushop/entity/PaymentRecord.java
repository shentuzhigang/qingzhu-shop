package cn.edu.zstu.qingzhu.qingzhushop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PaymentRecord对象", description = "")
public class PaymentRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    @ApiModelProperty(value = "金额")
    private String amount;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "支付方式")
    private String paymentWay;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime paymentTime;


}
