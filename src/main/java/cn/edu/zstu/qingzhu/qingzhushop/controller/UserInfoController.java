package cn.edu.zstu.qingzhu.qingzhushop.controller;

import cn.edu.zstu.qingzhu.qingzhushop.entity.AuthenticationUser;
import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import cn.edu.zstu.qingzhu.qingzhushop.entity.User;
import cn.edu.zstu.qingzhu.qingzhushop.entity.VueRouteConfig;
import cn.edu.zstu.qingzhu.qingzhushop.service.IUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-23 09:04
 */
@Controller
@RequestMapping("/user/info")
@Api(tags = "用户信息接口")
public class UserInfoController {
    @Autowired
    IUserService iUserService;

    /**
     *
     * @param authentication
     * @return
     */
    @GetMapping("")
    @ResponseBody
    public ResponseBean getCurrentUser(Authentication authentication) {
        AuthenticationUser authenticationUser=((AuthenticationUser) authentication.getPrincipal());
        User user = iUserService.getById(authenticationUser.getId());
        return ResponseBean.success(user);
    }
    @GetMapping("/menu")
    @ResponseBody
    public List<VueRouteConfig> getMenu(Authentication authentication) {
        AuthenticationUser authenticationUser=((AuthenticationUser) authentication.getPrincipal());
        return  iUserService.getVueRouteConfig(authenticationUser);
    }

    @PutMapping("")
    public ResponseBean updateHr(@RequestBody User user, Authentication authentication) {
        user.setId(((AuthenticationUser) authentication.getPrincipal()).getId());
        if (iUserService.updateById(user)) {
            SecurityContextHolder
                    .getContext()
                    .setAuthentication(
                            new UsernamePasswordAuthenticationToken(user,
                                    authentication.getCredentials(),
                                    authentication.getAuthorities()));
            return ResponseBean.ok("更新成功!");
        }
        return ResponseBean.error("更新失败!");
    }


    @PostMapping("/userface")
    public ResponseBean updateHrUserface(MultipartFile file, Authentication authentication) {
        AuthenticationUser user = (AuthenticationUser) authentication.getPrincipal();
        if (iUserService.updateUserface(file, user.getId()) == 1) {
            SecurityContextHolder
                    .getContext()
                    .setAuthentication(
                            new UsernamePasswordAuthenticationToken(user,
                                    authentication.getCredentials(),
                                    authentication.getAuthorities())
                    );
            return  ResponseBean.success(user);
        }
        return ResponseBean.error("更新失败!");
    }


}
