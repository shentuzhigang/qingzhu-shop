package cn.edu.zstu.qingzhu.qingzhushop.service.impl;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Activity;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.ActivityMapper;
import cn.edu.zstu.qingzhu.qingzhushop.service.IActivityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity>
        implements IActivityService {

    @Autowired
    private ActivityMapper activityMapper;

    @Override
    public Activity selectByActivityId(Integer id) {
        return activityMapper.selectByActivityId(id);
    }

    @Override
    public List<Activity> selectByActivityName(String activityName) {
        return activityMapper.selectByActivityName(activityName);
    }

    @Override
    public int updateByIdSelective(Activity activity) {
        return activityMapper.updateByIdSelective(activity);
    }

}
