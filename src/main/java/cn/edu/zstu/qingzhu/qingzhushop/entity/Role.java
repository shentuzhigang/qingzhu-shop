package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-03-07 18:39
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "角色")
public class Role implements Serializable {
    //ID
    @ApiModelProperty(name = "角色ID", value = "角色ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    //角色名
    @ApiModelProperty(name = "角色名", value = "角色名")
    private String name;
    //角色中文名
    @ApiModelProperty(name = "角色中文名", value = "角色中文名")
    private String nameZh;
}
