package cn.edu.zstu.qingzhu.qingzhushop.mapper;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Role;
import cn.edu.zstu.qingzhu.qingzhushop.entity.User;
import cn.edu.zstu.qingzhu.qingzhushop.entity.UserCount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import javafx.util.Pair;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-11-06
 */
@Mapper
@Component
public interface UserMapper extends BaseMapper<User> {
    @Select("select * from user where username=#{username}")
    User loadUserByUserName(String username);

    @Select("select * from role r,user_role ur where r.id=ur.rid and ur.uid=#{uid}")
    List<Role> getUserRolesByUid(String uid);

    List<User> getAllUsersWithRoles();

    List<User> getAllUsersWithRolesNoAuthenticationUserByKeyword(@Param("userId") Integer userId, @Param("keyword") String keyword);

    @Select("SELECT * FROM user_count_sum")
    List<UserCount> selectUserCount();

    @Select(" select count(0) from ( select date_format( `user`.`last_time`, #{format} ) AS `last_time_a` from user ) as lt where lt.last_time_a=#{date}")
    Integer getCountByDate(String format, String date);

    @Select("select address as `key`,count(1) as `value` from user group by address limit 4")
    List<Pair<String, Integer>> getCountByCityIdLimit4();

    List<User> selectByUserName(String userName);

    @Select("select * from user where phone=#{phone}")
    List<User> selectByUserPhone(String phone);
}
