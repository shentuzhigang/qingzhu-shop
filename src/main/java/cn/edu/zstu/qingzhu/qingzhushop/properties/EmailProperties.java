package cn.edu.zstu.qingzhu.qingzhushop.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-08-04 19:44
 */

@Data
@Component
@ConfigurationProperties(prefix = "email", ignoreUnknownFields = true)
public class EmailProperties {
    /**
     * 发件邮箱
     */
    private String emailFrom;
    private String emailTo;

    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }
}
