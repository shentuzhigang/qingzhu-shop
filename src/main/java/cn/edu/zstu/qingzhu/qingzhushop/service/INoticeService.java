package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Notice;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-08-19
 */
public interface INoticeService extends IService<Notice> {

    List<Notice> listNoticeWithReceiver();

    List<Notice> getUnpublishedNotice();
}
