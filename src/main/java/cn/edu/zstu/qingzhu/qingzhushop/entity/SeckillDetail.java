package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * FileName: SeckillDetail
 *
 * @Author: LvYibin
 * @Date: 2021/5/17 08:55
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SeckillDetail对象", description = "")
public class SeckillDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(name = "SeckillDetail ID", value = "SeckillDetail ID", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "金额")
    private String amount;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "数量")
    @TableField(value = "totalnumber")
    private Integer totalnumber;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "顾客ID")
    private Integer customerId;

    @ApiModelProperty(value = "物品ID")
    private Integer SeckillId;

    @ApiModelProperty(value = "商家ID")
    private Integer employeeId;


    @ApiModelProperty(value = "信息")
    private String message;


    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}