package cn.edu.zstu.qingzhu.qingzhushop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-26 14:14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "元")
public class Meta {
    @ApiModelProperty(name = "标题", value = "标题")
    private String title;
    @ApiModelProperty(name = "链接", value = "链接")
    private String link;
    @ApiModelProperty(name = "图标", value = "图标")
    private String icon;
    @ApiModelProperty(name = "保持", value = "保持")
    private Boolean keepAlive;
    @ApiModelProperty(name = "是否需要授权", value = "是否需要授权")
    private Boolean requireAuth;
}
