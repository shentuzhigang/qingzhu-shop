package cn.edu.zstu.qingzhu.qingzhushop.configure;


import cn.edu.zstu.qingzhu.qingzhushop.properties.UCloudProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-30 15:13
 */
@EnableConfigurationProperties(UCloudProperties.class)
public class UCloudConfiguration {
}
