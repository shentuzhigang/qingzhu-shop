package cn.edu.zstu.qingzhu.qingzhushop.task;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Notice;
import cn.edu.zstu.qingzhu.qingzhushop.service.INoticeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-08-07 11:35
 */
@Slf4j
@Component
public class NoticePublishTask {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    INoticeService iNoticeService;

    @Scheduled(cron = "0 0/1 * * * ?")
    public void publishNotice() {
        log.info("通知发布");
        List<Notice> unpublishedNotices = iNoticeService.getUnpublishedNotice();
        for (Notice notice : unpublishedNotices) {
            simpMessagingTemplate.convertAndSend("/topic", notice);
        }
        simpMessagingTemplate.convertAndSend("/topic", "系统通知");
    }
}
