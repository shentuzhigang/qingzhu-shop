package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.AuthenticationUser;
import cn.edu.zstu.qingzhu.qingzhushop.entity.Order;
import cn.edu.zstu.qingzhu.qingzhushop.entity.OrderDetail;
import cn.edu.zstu.qingzhu.qingzhushop.entity.ShoppingCart;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-07 21:28
 */
public interface IOrderService extends IService<Order> {

    Order selectByOrderId(Integer id);

    List<Order> selectByOrderCode(String orderCode);

    int updateByIdSelective(Order order);

    List<Order> selectByUserId(Integer userId);

    OrderDetail getOrderDetailByOrderNo(String orderNo, Integer id);

    Page<Order> listOrderByUserId(Integer id, Integer page, Integer size);

    Object saveOrder(AuthenticationUser user, List<ShoppingCart> shoppingCartList);

    String cancelOrder(String orderNo, Integer id);

    String finishOrder(String orderNo, Integer id);

    String paySuccess(String orderNo, int payType);

    Integer submitOrder(Order order, List<Integer> goods);

    String sumSalesVolume();

    String sumSales();

    int paySuccess(Integer orderId) throws Exception;
}
