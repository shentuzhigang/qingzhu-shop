package cn.edu.zstu.qingzhu.qingzhushop.controller;

import cn.edu.zstu.qingzhu.qingzhushop.common.ResponseBeanMessage;
import cn.edu.zstu.qingzhu.qingzhushop.entity.AuthenticationUser;
import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import cn.edu.zstu.qingzhu.qingzhushop.entity.ShoppingCart;
import cn.edu.zstu.qingzhu.qingzhushop.service.IShoppingCartService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
@Api(tags = "用户购物车接口")
@CrossOrigin(origins={"*"}, methods={RequestMethod.GET, RequestMethod.POST})
public class ShoppingCartController {

    @Autowired
    IShoppingCartService iShoppingCartService;

    //加入购物车
    @PostMapping("/addGoods")
    @ResponseBody
    public ResponseBean addCart(@RequestBody ShoppingCart shoppingCart){
        System.out.println(shoppingCart.getId());
        System.out.println(shoppingCart.getGoodsNum());
        System.out.println(shoppingCart.getGoodsId());
        ShoppingCart shoppingCart1 = iShoppingCartService.listShoppingCartUserIdAndGoodsId(shoppingCart.getUid(),shoppingCart.getGoodsId());
        if (null == shoppingCart1){
            if(iShoppingCartService.save(shoppingCart)){
                return ResponseBean.success();
            }else{
                return ResponseBean.error();
            }
        }else {
            shoppingCart1.setGoodsNum(shoppingCart1.getGoodsNum() + shoppingCart.getGoodsNum());
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("id",shoppingCart1.getId());
            if(iShoppingCartService.update(shoppingCart1,wrapper)){
                return ResponseBean.success();
            }else{
                return ResponseBean.error();
            }
        }
    }

    @GetMapping("/")
    public ResponseBean cartListPage(Authentication authentication) {
        AuthenticationUser user = (AuthenticationUser) authentication.getPrincipal();
        List<ShoppingCart> myShoppingCarts = iShoppingCartService.listShoppingCartUserId(user.getId());
        return ResponseBean.success("查询成功",myShoppingCarts);
    }

    @PostMapping("/")
    @ResponseBody
    public ResponseBean saveShoppingCart(@RequestBody ShoppingCart ShoppingCart,
                                       Authentication authentication) {
        AuthenticationUser user = (AuthenticationUser) authentication.getPrincipal();
        ShoppingCart.setId(user.getId());
        //添加成功
        if (iShoppingCartService.save(ShoppingCart)) {
            return ResponseBean.success(ShoppingCart);
        }
        //添加失败
        return ResponseBean.error();
    }

    @PutMapping("/")
    @ResponseBody
    public ResponseBean updateShoppingCart(@RequestBody ShoppingCart ShoppingCart,
                                         Authentication authentication) {
        AuthenticationUser user = (AuthenticationUser) authentication.getPrincipal();
        ShoppingCart.setId(user.getId());
        //todo 判断数量
        //修改成功
        if (iShoppingCartService.updateById(ShoppingCart)) {
            return ResponseBean.success(ShoppingCart);
        }
        //修改失败
        return ResponseBean.error();
    }

    @DeleteMapping("/{ShoppingCartId}")
    @ResponseBody
    public ResponseBean deleteShoppingCart(@PathVariable("ShoppingCartId") Long ShoppingCartId) {
//        AuthenticationUser user = (AuthenticationUser) authentication.getPrincipal();
        Boolean deleteResponseBean = iShoppingCartService.removeById(ShoppingCartId);
        //删除成功
        if (deleteResponseBean) {
            return ResponseBean.success();
        }
        //删除失败
        return ResponseBean.error(ResponseBeanMessage.OPERATE_ERROR);
    }

    @GetMapping("/settle")
    public ResponseBean settlePage(Authentication authentication) {
        BigDecimal priceTotal = new BigDecimal(0);
        AuthenticationUser user = (AuthenticationUser) authentication.getPrincipal();
        List<ShoppingCart> myShoppingCarts = iShoppingCartService.listOrderByUserId(user.getId());
        if (CollectionUtils.isEmpty(myShoppingCarts)) {
            //无数据则不跳转至结算页
            return ResponseBean.error(ResponseBeanMessage.OPERATE_ERROR);
        } else {
            //总价
            for (ShoppingCart shop : myShoppingCarts) {
                priceTotal = priceTotal.add(shop.getGoods().getGoodsPrice().multiply(new BigDecimal(shop.getGoodsNum())));
               // priceTotal += ShoppingCart.getGoodsCount() * ShoppingCart.getSellingPrice();
            }
            if (priceTotal.compareTo(new BigDecimal(0))<0) {
                return ResponseBean.error(ResponseBeanMessage.OPERATE_ERROR);
            }
        }
        return ResponseBean.success(myShoppingCarts);
    }
    
}