package cn.edu.zstu.qingzhu.qingzhushop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Banner对象", description = "")
public class Banner implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    private Integer id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "图片URL")
    private String src;

    @ApiModelProperty(value = "链接URL")
    private String link;


}
