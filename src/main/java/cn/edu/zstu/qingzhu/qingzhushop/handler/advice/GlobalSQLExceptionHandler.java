package cn.edu.zstu.qingzhu.qingzhushop.handler.advice;

import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-03-21 16:51
 */
@RestControllerAdvice
public class GlobalSQLExceptionHandler {
    @ExceptionHandler(SQLException.class)
    public ResponseBean sqlException(SQLException e) {
        if (e instanceof SQLIntegrityConstraintViolationException) {
            return ResponseBean.error("该数据有关联数据，操作失败!");
        }
        return ResponseBean.error("数据库异常，操作失败!");
    }
}