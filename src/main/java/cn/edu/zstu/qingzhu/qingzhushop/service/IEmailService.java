package cn.edu.zstu.qingzhu.qingzhushop.service;

import javafx.util.Pair;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-02-15 21:29
 */
public interface IEmailService {
    /**
     * 发送简单邮件
     *
     * @param to      收件人地址
     * @param subject 邮件标题
     * @param content 邮件内容
     */
    void sendSimpleMail(String to, String subject, String content);

    /**
     * 发送简单邮件
     *
     * @param to          收件人地址
     * @param subject     邮件标题
     * @param content     邮件内容
     * @param attachments <文件名，附件> 附件列表
     */
    void sendAttachmentsMail(String to, String subject, String content, List<Pair<String, File>> attachments);

    /**
     * 发送模板邮件
     *
     * @param template    模板
     * @param to          收件人地址
     * @param subject     邮件标题
     * @param content     <key, 内容> 邮件内容
     * @param attachments <文件名，附件> 附件列表
     */
    void sendTemplateMail(String template, String to, String subject, Map<String, Object> content, List<Pair<String, File>> attachments);
}
