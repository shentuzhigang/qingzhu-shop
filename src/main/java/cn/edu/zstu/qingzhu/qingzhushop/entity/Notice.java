package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author ShenTuZhiGang
 * @since 2020-08-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Notice对象", description = "")
public class Notice implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "消息接收对象（0是给全体人的消息，其他是针对某个用户UID）")
    private Integer receiveUid;

    @ApiModelProperty(value = "用户")
    @TableField(exist = false)
    private User receiver;

    @ApiModelProperty(value = "消息标题")
    private String title;

    @ApiModelProperty(value = "消息内容")
    private String content;

    @ApiModelProperty(value = "发送时间")
    private LocalDateTime receiveTime;

    @ApiModelProperty(value = "用户是否阅读过消息")
    @TableField(value = "`read`")
    private Boolean read;

    @ApiModelProperty(value = "是否被删除")
    @TableField(value = "`delete`")
    private Boolean delete;

    @ApiModelProperty(value = "预定发送时间")
    private LocalDateTime sendTime;

    @ApiModelProperty(value = "状态")
    private Integer status;
}
