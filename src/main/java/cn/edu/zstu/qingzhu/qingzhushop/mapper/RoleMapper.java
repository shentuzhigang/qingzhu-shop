package cn.edu.zstu.qingzhu.qingzhushop.mapper;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-03-07 19:26
 */

@Mapper
@Component
public interface RoleMapper extends BaseMapper<Role> {
    @Select("SELECT * FROM role WHERE id=#{id}")
    Role loadRolesById(String id);

    @Select("select * from role r,user_role ur where r.id=ur.rid and ur.uid=#{uid}")
    List<Role> getUserRolesByUid(Integer uid);

    @Select("SELECT * FROM resource_role rr JOIN role r ON rr.rid=r.id WHERE rr.sid=#{resourceId}")
    List<Role> loadRolesByResourceId(String resourceId);
}
