package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-25 13:37
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "用户角色映射")
public class UserRole {
    //ID
    @ApiModelProperty(name = "用户角色映射ID", value = "用户角色映射ID")
    @TableId(value = "id", type = IdType.AUTO)
    private int id;
    //角色ID
    @ApiModelProperty(name = "角色ID", value = "角色ID")
    private int rid;

    //用户ID
    @ApiModelProperty(name = "用户ID", value = "用户ID")
    private int uid;

}
