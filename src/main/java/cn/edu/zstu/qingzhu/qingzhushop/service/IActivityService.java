package cn.edu.zstu.qingzhu.qingzhushop.service;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Activity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-07 21:08
 */
public interface IActivityService extends IService<Activity> {

    Activity selectByActivityId(Integer id);

    List<Activity> selectByActivityName(String activityName);

    int updateByIdSelective(Activity activity);
}
