package cn.edu.zstu.qingzhu.qingzhushop.controller.admin;

import cn.edu.zstu.qingzhu.qingzhushop.controller.BaseRestController;
import cn.edu.zstu.qingzhu.qingzhushop.entity.GoodsCategory;
import cn.edu.zstu.qingzhu.qingzhushop.entity.ResponseBean;
import cn.edu.zstu.qingzhu.qingzhushop.service.IGoodsCategoryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-22 20:58
 */
@RestController
@RequestMapping("/admin/goodsCategory")
@Api(tags = "商品类型管理接口")
public class GoodsCategoryManagerController extends BaseRestController<IGoodsCategoryService, GoodsCategory,Integer> {
    @Autowired
    private IGoodsCategoryService iGoodsCategoryService;

    //获取所有活动
    @GetMapping("/list")
    @ResponseBody
    public ResponseBean getGoodsCategoryList() {
        List<GoodsCategory> activitiesList = iGoodsCategoryService.list();
        return ResponseBean.success("GoodsCategory", activitiesList);
    }

}
