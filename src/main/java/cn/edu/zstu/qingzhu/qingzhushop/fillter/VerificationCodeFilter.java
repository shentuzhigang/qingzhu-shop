package cn.edu.zstu.qingzhu.qingzhushop.fillter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-23 19:49
 */
//@Component
//@WebFilter(value = "/doLogin")
public class VerificationCodeFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        if ("POST".equals(req.getMethod())
                && "/doLogin".equals(req.getServletPath())) {
            filterChain.doFilter(req, resp);
            return;/*
            //登录请求
            String code = req.getParameter("code");
            String verify_code = (String) req.getSession().getAttribute("verify_code");
            if (code == null
                    || verify_code == null
                    || "".equals(code)
                    || !verify_code.toLowerCase().equals(code.toLowerCase())) {
                //验证码不正确
                resp.setContentType("application/json;charset=utf-8");
                PrintWriter out = resp.getWriter();
                out.write(new ObjectMapper()
                        .writeValueAsString(ResponseBean.error("验证码填写错误")));
                out.flush();
                out.close();
                return;
            } else {
                filterChain.doFilter(req, resp);
            }*/
        } else {
            filterChain.doFilter(req, resp);
        }
    }
}
