package cn.edu.zstu.qingzhu.qingzhushop.entity;

import lombok.Data;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-08 19:41
 */
@Data
public class GoodsSearchDTO {
    private QueryModel queryModel;
    private Integer goodsCategoryId;
    private String keyword;
}
