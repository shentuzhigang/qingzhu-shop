package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * FileName: Seckill
 *
 * @Author: LvYibin
 * @Date: 2021/5/17 08:48
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Seckill implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private BigDecimal amount;                 //金额

    private Integer total;                     //总数

    private String onlyseckill;                  //唯一标识符

    private Integer isActive;                  //是否可用，1可用，0不可用，默认为0

    private LocalDateTime createTime;                   //创建时间

    private String continuetime;             //持续时间


}