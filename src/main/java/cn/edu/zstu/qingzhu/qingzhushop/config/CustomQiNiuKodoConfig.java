package cn.edu.zstu.qingzhu.qingzhushop.config;

import cn.edu.zstu.qingzhu.qingzhushop.properties.QiNiuProperties;
import com.google.gson.Gson;
import com.qiniu.http.Client;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

interface QiNiuRegion {
    String REGION_0 = "region0";
    String REGION_1 = "region1";
    String REGION_2 = "region2";
    String REGION_Na = "regionNa";
    String REGION_AS = "regionAs";
    String HUA_DONG = "huadong";
    String HUA_BEI = "huabei";
    String HUA_NAN = "huanan";
    String BRI_MEI = "beimei";
    String XIN_JIA_PO = "xinijipo";
}

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-29 20:27
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(QiNiuProperties.class)
public class CustomQiNiuKodoConfig {
    @Value("${qiniu.access-key}")
    private String accessKey;
    @Value("${qiniu.secret-key}")
    private String secretKey;
    @Value("${qiniu.region}")
    private String region;

    /**
     * 配置自己空间所在的区域
     *
     * @return
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    @Bean
    public com.qiniu.storage.Configuration qiniuConfig()
            throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Field[] fields = QiNiuRegion.class.getDeclaredFields();
        for (Field field : fields) {
            String r = (String) field.get(QiNiuRegion.class);
            if (r.equals(region)) {
                log.debug("七牛云对象存储Region对象：" + r);
                com.qiniu.storage.Configuration c = new com.qiniu.storage.Configuration(
                        (Region) Region.class
                                .getMethod(r, null)
                                .invoke(null, null));
                return c;
            }
        }
        return new com.qiniu.storage.Configuration(Region.huadong());
    }

    /**
     * 构建一个七牛上传工具实例
     *
     * @param c
     * @return
     */
    @Bean
    public UploadManager uploadManager(com.qiniu.storage.Configuration c) {
        return new UploadManager(c);
    }


    /**
     * 认证信息实例
     *
     * @return
     */
    @Bean
    public Auth auth() {
        return Auth.create(accessKey, secretKey);
    }

    /**
     * @param c com.qiniu.storage.Configuration
     * @return
     */
    @Bean
    public Client client(com.qiniu.storage.Configuration c) {
        return new Client(c);
    }

    /**
     * 构建七牛空间管理实例
     *
     * @param auth 认证信息
     * @param c    com.qiniu.storage.Configuration
     * @return
     */
    @Bean
    public BucketManager bucketManager(Auth auth, com.qiniu.storage.Configuration c) {
        return new BucketManager(auth, c);
    }

    /**
     * Gson
     *
     * @return
     */
    @Bean
    public Gson gson() {
        return new Gson();
    }
}