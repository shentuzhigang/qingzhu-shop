package cn.edu.zstu.qingzhu.qingzhushop.service.impl;

import cn.edu.zstu.qingzhu.qingzhushop.entity.Role;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.ResourceRoleMapper;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.RoleMapper;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.UserRoleMapper;
import cn.edu.zstu.qingzhu.qingzhushop.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-07-23 11:26
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role>
        implements IRoleService {
    @Autowired
    RoleMapper roleMapper;
    @Autowired
    ResourceRoleMapper resourceRoleMapper;
    @Autowired
    UserRoleMapper userRoleMapper;

    @Override
    public List<Integer> getResourceIdsByRoleId(Integer roleId) {
        return resourceRoleMapper.getResourceIdsByRoleId(roleId);
    }

    @Override
    @Transactional
    public List<Integer> updateResourceRole(Integer roleId, Integer[] resourceIds) {
        Map<String, Object> map = new HashMap<>();
        map.put("rid", roleId);
        resourceRoleMapper.deleteByMap(map);
        resourceRoleMapper.insertResourceRole(roleId, resourceIds);
        return resourceRoleMapper.getResourceIdsByRoleId(roleId);
    }
}
