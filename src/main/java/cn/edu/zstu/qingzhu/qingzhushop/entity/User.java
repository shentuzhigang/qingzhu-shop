package cn.edu.zstu.qingzhu.qingzhushop.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-07-21 15:27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "用户", description = "用户表")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    //ID
    @ApiModelProperty(name = "用户ID", value = "用户ID", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    //用户名
    @ApiModelProperty(value = "用户名", example = "xizi")
    private String username;
    //用户密码
    @JsonIgnore
    @ApiModelProperty(value = "用户密码")
    private String password;
    //姓名
    @ApiModelProperty(value = "昵称", example = "西湖")
    private String nickname;
    //个性签名
    @ApiModelProperty(value = "个性签名", example = "杭州青竹商城欢迎您")
    private String signature;

    @ApiModelProperty(value = "性别", example = "杭州青竹商城欢迎您")
    private String sex;

    @ApiModelProperty(value = "年龄", example = "18")
    private Integer age;
    //是否可用
    @ApiModelProperty(value = "是否可用", example = "true")
    private Boolean enable;
    //是否锁定
    @ApiModelProperty(value = "是否锁定", example = "true")
    private Boolean locked;
    //电话
    @ApiModelProperty(value = "电话", example = "84600000")
    private String phone;
    //移动电话
    @ApiModelProperty(value = "移动电话", example = "13800000000")
    private String telephone;
    //地址
    @ApiModelProperty(value = "地址", example = "浙江省杭州市")
    private String address;
    //用户头像
    @ApiModelProperty(value = "用户头像", example = "https://example.cn/1")
    private String userface;
    //注册时间
    @ApiModelProperty(value = "注册时间", example = "1583107200000", dataType = "Timestamp")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    //最后活跃时间
    @ApiModelProperty(value = "最后活跃时间", example = "1583107200000", dataType = "Timestamp")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    //备注
    @ApiModelProperty(value = "备注", example = "备注")
    private String remark;
//    //角色列表
//    @ApiModelProperty(value = "角色列表")
//    @TableField(exist = false)
//    private List<Role> roles;
}
