package cn.edu.zstu.qingzhu.qingzhushop;

import cn.edu.zstu.qingzhu.qingzhushop.entity.*;
import cn.edu.zstu.qingzhu.qingzhushop.mapper.*;
import cn.edu.zstu.qingzhu.qingzhushop.rabbitmq.DeadInfo;
import cn.edu.zstu.qingzhu.qingzhushop.rabbitmq.publisher.BasicPublisher;
import cn.edu.zstu.qingzhu.qingzhushop.rabbitmq.publisher.DeadPublisher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
class QingzhuShopApplicationTests {

    @Autowired
    OrderMapper orderMapper;
    @Autowired
    GoodsMapper goodsMapper;
    @Autowired
    UserRoleMapper userRoleMapper;
    @Autowired
    ShoppingCartMapper shoppingCartMapper;
    @Test
    void contextLoads() {
        ShoppingCart shoppingCart = shoppingCartMapper.selectByCartId(1);
        System.out.println(shoppingCart.getUpdateTime().toInstant(ZoneOffset.of("+8")).toEpochMilli());
    }

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private BasicPublisher basicPublisher;
    @Autowired
    private DeadPublisher deadPublisher;
//    @Test
    public void basicrabbitmqtest() throws Exception{
        DeadInfo d1 = new DeadInfo(1,"lalala123");
        DeadInfo d2 = new DeadInfo(2,"吕羿滨");
        List<DeadInfo> list = new ArrayList();
        list.add(d1);
        list.add(d2);
        String json = objectMapper.writeValueAsString(d1);
        String js = objectMapper.writeValueAsString(d2);
        //list.forEach(System.out::println);
        deadPublisher.sendMsg(json);
        deadPublisher.sendMsg(js);
        Thread.sleep(30000);
    }

}
