#DockerFile内容
#基础镜像，如果本地没有，会从远程仓库拉取。
FROM mjalas/javafx

MAINTAINER Starzkg

# 修改编码
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# 统一容器与服务器时间
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Spring Boot 环境设置
ENV SPRING_PROFILES_ACTIVE=test

# 复制当前项目target/spring-boot-demo.jar到容器中
COPY ./qingzhu-shop-0.0.1-SNAPSHOT.jar qingzhu-shop-web.jar

# 开放端口
EXPOSE 80

ENTRYPOINT ["java","-Dfile.encoding=utf-8","-jar","qingzhu-shop-web.jar"]